import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [

    /*
    {
      path: '/errors',
      name: 'errors-page',
      component: require('@/components/ErrorsPage').default
    },
    */
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/HomePage').default
    }

  ]
})
