import { app, BrowserWindow } from 'electron'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}



let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

  
function createWindow () {
  /**
   * Initial window options
   */

  mainWindow = new BrowserWindow({

    height: 563,
    frame: false,
    useContentSize: true,
    width: 1000,
    webPreferences: {
      webSecurity: false,
      nodeIntegration: true // add this
    }
  })

  const { ipcMain } = require('electron')

    // 'win' as the BrowserWindow instance
  mainWindow.on('resize', function () {
    setTimeout(function () {


      mainWindow.send('Get:Document_sizes');
    }, 100);
  });

  mainWindow.loadURL(winURL)

  ipcMain.on('Update:Document_sizes', function(event, data){

    mainWindow.setSize(data.width, data.height);

  });

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
