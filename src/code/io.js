import  path from 'path';
const {dialog} = require('electron').remote;
const app = require('electron').remote.app;

import fs from 'fs';

var base_path = path.join( app.getAppPath() , "/.." );
const PROGRAM_PATH = path.join( app.getAppPath() , "/..");

function locateMangaFolder(ruta)  {
    var manga_path = dialog.showOpenDialog({
        properties: ['openDirectory']
    });

    io.save_file( {
        "manga_path": path.relative( PROGRAM_PATH, manga_path[0] ),
        "base_path": PROGRAM_PATH
    }, ruta);
}

function loadLocalInfo () {
    var ruta = __static+"/localInfo.json";

    // Check that the file exists locally
    if( !fs.existsSync(ruta) ) {
        console.log("File not found");
        console.log(app.getAppPath());
        locateMangaFolder(ruta);
    } 

    let rawdata = fs.readFileSync(ruta);
    var data = JSON.parse(rawdata);

    
    if( path.isAbsolute(data.manga_path) ) {

        base_path = data.manga_path
  
      } else {
  
        base_path = path.resolve( path.join( PROGRAM_PATH, data.manga_path ) );
  
      }

    data.manga_full_path = base_path;

    return data;
}

function load_json(ruta) {

    if( !fs.existsSync(ruta) ) {
        console.log("File not found");
        save_file( {} , ruta);
    }

    let rawdata = fs.readFileSync(ruta);
    var data = JSON.parse(rawdata);   

    return data;
}

function save_file( data , ruta ) {
    console.log("Saving file "+ ruta);
    var json = JSON.stringify(data, null, "\t");
    fs.writeFileSync( ruta, json, {'encoding':'utf8', 'flag':'w' });
}

function read_manga (  )  { 
    var local_info = loadLocalInfo();
    var manga_full_path = local_info.manga_full_path;

    var tmp_dir = process.cwd();
    process.chdir( manga_full_path );

    var data = load_json( "updater.json" );

    process.chdir( tmp_dir );

    return data;
}

function read_errors (  )  { 
    var local_info = loadLocalInfo();
    var manga_full_path = local_info.manga_full_path;

    var tmp_dir = process.cwd();
    process.chdir( manga_full_path );

    var data = load_json( "errors.json" );

    process.chdir( tmp_dir );

    return data;
}

function read_manga_data( normalized_name ) {
    var local_info = loadLocalInfo();
    var manga_full_path = local_info.manga_full_path;
    
    var tmp_dir = process.cwd();
    process.chdir( manga_full_path );

    var data = load_json( normalized_name + "/updater.json" );

    process.chdir( tmp_dir );

    return data;
}

function save_manga_data( data ) {
    var normalized_name = data.normalized_name;

    var local_info = loadLocalInfo();
    var manga_full_path = local_info.manga_full_path;
    
    var tmp_dir = process.cwd();
    process.chdir( manga_full_path );

    var data = save_file( data, normalized_name + "/updater.json" );

    process.chdir( tmp_dir );

    return data;
}

var io = { 
    getMangaList : () => {},

    loadLocalInfo,
    save_file,
    locateMangaFolder,
    read_manga,
    read_errors,
    read_manga_data,
    save_manga_data,
    getBasePath() { return base_path }


};

export default  io;